#pragma once

#include <memory>
#include "../../domain/matrix.h"

namespace helpers {
  namespace validators {
    bool is_valid_adjecency_matrix(
      std::shared_ptr<domain::Matrix<std::size_t>> matrix
    );
  }
}
