
#include <memory>
#include "../../domain/matrix.h"
#include "validators.h"

namespace helpers {
  namespace validators {
    bool is_valid_adjecency_matrix(
      std::shared_ptr<domain::Matrix<std::size_t>> matrix
    ) {
      if (matrix->get_height() != matrix->get_width()) {
        return false;
      }

      return true;
    }
  }
}
