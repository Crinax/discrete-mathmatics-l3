#pragma once

#include <iostream>
#include <sstream>
#include <string>

namespace helpers {
  template<typename T>
  std::string string_center(T value, std::size_t count, std::string symbol = " ") {
    std::stringstream result;
    std::string symbols_pre = "";
    std::string symbols_after = "";
    std::string value_str = std::to_string(value);

    for (std::size_t i = 0; i < count; i++) {
      symbols_after += symbol;
    }

    long int int_count = count - value_str.size();
    for (long int i = 0; i < int_count + 1; i++) {
      symbols_pre += symbol;
    }

    result << symbols_pre << value << symbols_after;

    return result.str();
  }
}
