#pragma once

#include <vector>
#include <string>
#include <sstream>
#include "../../parser/parser.h"

namespace helpers {
  template<typename ToType = std::string>
  std::vector<ToType> split(std::string input, char delim) {
    Parser parser = {};
    std::stringstream stream;
    std::string tmp;
    std::vector<ToType> result;
    
    stream << input;

    while (std::getline(stream, tmp, delim)) {
      result.push_back(*parser.parse<ToType>(tmp).get());
    }

    return result;
  }
}
