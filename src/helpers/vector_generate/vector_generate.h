#pragma once

#include <vector>
#include <functional>

namespace helpers {
  template<typename T>
  std::vector<T> vector_generate(
    std::function<T(std::size_t index)> generator,
    std::size_t size
  ) {
    std::vector<T> result = {};

    for (std::size_t index = 0; index < size; index++) {
      result.push_back(generator(index));
    }

    return result;
  }
}
