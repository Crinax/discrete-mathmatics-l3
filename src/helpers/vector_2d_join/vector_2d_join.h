#pragma once

#include <vector>
#include <sstream>
#include <string>
#include <iostream>

namespace helpers {
  template<typename TValue>
  std::string vector_2d_join(
    std::vector<std::vector<TValue>> vec,
    std::string horizontal_sep = ", ",
    std::string vertical_sep = "\n"
  ) {
    std::size_t width;
    std::size_t height = vec.size();
    std::stringstream result;

    for (int i = 0; i < height; i++) {
      width = vec.at(i).size();

      for (int j = 0; j < width; j++) {
        result << vec.at(i).at(j);
        
        if (width - j > 1) {
          result << horizontal_sep;
        }
      }

      if (height - i > 1) {
        result << vertical_sep;
      }
    }

    return result.str();
  }
}
