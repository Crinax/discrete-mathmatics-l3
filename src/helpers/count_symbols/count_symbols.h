#pragma once

#include <string>

namespace helpers {
  inline std::size_t count_symbols(std::string input, char symbol) {
    std::size_t result = 0;

    for (std::string::size_type i = 0; i < input.size(); i++) {
      if (input.at(i) == symbol) {
        result++;
      }
    }

    return result;
  }
}
