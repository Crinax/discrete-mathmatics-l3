#pragma once

#include <exception>
#include <memory>
#include <string>
#include "./clear_line.h"
#include "../parser/parser.h"

namespace console {
  template<typename TOutput, typename... Args>
  std::unique_ptr<TOutput> prompt(std::string message = "") {
    Parser parser;
    std::unique_ptr<TOutput> result;
    std::string input;

    while (true) {
      try {
        std::cout << message;
        std::getline(std::cin, input);
        result = parser.parse<TOutput>(input);
        break;
      } catch(std::exception) {
        console::clear_line();
      }
    }

    return std::move(result);
  }
}
