#pragma once

#include <memory>
#include <vector>
#include "../domain/matrix.h"

namespace adapters {
  inline std::shared_ptr<domain::Matrix<short int>> create_incidence_matrix() {
    return std::shared_ptr<domain::Matrix<short int>>(
      domain::Matrix<short int>::empty()
    );
  }

  inline std::shared_ptr<domain::Matrix<short int>> create_incidence_matrix(
    std::vector<std::vector<short int>> vec
  ) {
    return std::shared_ptr<domain::Matrix<short int>>(
      domain::Matrix<short int>::from_2d_vector(vec)
    );
  }
}
