#pragma once

#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include "../domain/matrix.h"
#include "../helpers/validators/validators.h"
#include "../helpers/vector_generate/vector_generate.h"

namespace adapters {
  namespace converters {
    inline std::shared_ptr<domain::Matrix<long int>> convert_adjecency_matrix_to_incidence(
      std::shared_ptr<domain::Matrix<std::size_t>> adjecency_matrix
    ) {
      if (!helpers::validators::is_valid_adjecency_matrix(adjecency_matrix)) {
        throw std::invalid_argument("Argument must be a adjecency matrix");
      }

      std::shared_ptr<domain::Matrix<long int>> result = 
        std::shared_ptr<domain::Matrix<long int>>(
          domain::Matrix<long int>::empty()
        );
      std::size_t matrix_height = adjecency_matrix->get_width();
      std::size_t matrix_width = adjecency_matrix->get_height();

      for (
        std::size_t row = 0;
        row < matrix_height;
        row++
      ) {
        for (
          std::size_t column = row;
          column < matrix_width;
          column++
        ) {
          std::pair<std::size_t, std::size_t> pair = {
            adjecency_matrix->at(row, column),
            adjecency_matrix->at(column, row)
          };

          if (pair.second == 0 && pair.first == 0) {
            continue;
          }

          if (pair.first == pair.second) {
            result->push_column(
              helpers::vector_generate<long int>(
                [row, column, pair](auto index) -> long int {
                  return index == row || index == column
                    ? pair.first
                    : 0;
                },
                matrix_height
              )
            );

            continue;
          }
          
          if (pair.first > 0 && pair.second == 0) {
            result->push_column(
              helpers::vector_generate<long int>(
                [row, column, pair](auto index) -> long int {
                  return index == row
                    ? pair.first
                    : 0;
                },
                matrix_height
              )
            );

            continue;
          }

          if (pair.second > 0 && pair.first == 0) {
            result->push_column(
              helpers::vector_generate<long int>(
                [row, column, pair](auto index) -> long int {
                  return index == column
                    ? pair.second
                    : 0;
                },
                matrix_height
              )
            );

            continue;
          }

          result->push_column(
            helpers::vector_generate<long int>(
              [row, column, pair](auto index) -> long int {
                return index == row
                    ? pair.first
                    : index == column
                      ? -pair.first
                      : 0; 
              },
              matrix_height
            )
          );

          result->push_column(
            helpers::vector_generate<long int>(
              [row, column, pair](auto index) -> long int {
                return index == row
                    ? -pair.second
                    : index == column
                      ? pair.second
                      : 0; 
              },
              matrix_height
            )
          );
        }
      }

      return result;
    }

    typedef std::vector<std::vector<std::size_t>> adjecency_list;
    inline std::shared_ptr<adjecency_list> convert_adjacency_matrix_to_list(
      std::shared_ptr<domain::Matrix<std::size_t>> matrix
    ) {
      std::size_t height = matrix->get_height();
      std::size_t width = matrix->get_width();
      std::shared_ptr<adjecency_list> result =
        std::make_shared<adjecency_list>();

      for (std::size_t row = 0; row < height; row++) {
        std::vector<std::size_t> list = {};

        for (std::size_t column = 0; column < width; column++) {
          if (matrix->at(row, column) > 0) {
            list.push_back(column + 1);
          }
        }

        result->push_back(list);
      }

      return result;
    }

    typedef std::vector<std::vector<std::size_t>> incidence_list;
    inline std::shared_ptr<incidence_list> convert_incidence_matrix_to_list(
      std::shared_ptr<domain::Matrix<long int>> matrix
    ) {
      std::size_t height = matrix->get_height();
      std::size_t width = matrix->get_width();
      std::shared_ptr<incidence_list> result =
        std::make_shared<adjecency_list>();

      for (std::size_t row = 0; row < height; row++) {
        std::vector<std::size_t> list = {};

        for (std::size_t column = 0; column < width; column++) {
          if (matrix->at(row, column) != 0) {
            list.push_back(column + 1);
          }
        }

        result->push_back(list);
      }

      return result;
    }
  }
}
