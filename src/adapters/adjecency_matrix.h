#pragma once

#include <memory>
#include <vector>
#include "../domain/matrix.h"

namespace adapters {
  inline std::shared_ptr<domain::Matrix<std::size_t>> create_adjecency_matrix() {
    return std::shared_ptr<domain::Matrix<std::size_t>>(
      domain::Matrix<std::size_t>::empty()
    );
  }

  inline std::shared_ptr<domain::Matrix<std::size_t>> create_adjecency_matrix(
    std::vector<std::vector<std::size_t>> vec
  ) {
    return std::shared_ptr<domain::Matrix<std::size_t>>(
      domain::Matrix<std::size_t>::from_2d_vector(vec)
    );
  }
}
