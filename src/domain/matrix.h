#pragma once

#include <sstream>
#include <vector>
#include <functional>
#include <stdexcept>
#include "../helpers/string_center/string_center.h"

namespace domain {
  template<typename T>
  class Matrix {
    protected:
      Matrix() {
        this->_column_size = 0;
        this->_matrix = {};
      }

      Matrix(std::vector<std::vector<T>> vec) {
        this->_column_size = vec.at(0).size();
        this->_matrix = vec;
      };

      std::size_t _column_size;
      std::vector<std::vector<T>> _matrix;

    public:
      static Matrix<T>* from_2d_vector(std::vector<std::vector<T>>& vec) {
        std::size_t column_size = vec.at(0).size();

        for (std::size_t i = 1; i < vec.size(); i++) {
          if (vec.at(i).size() != column_size) {
            throw std::invalid_argument("Rows should have same length!");
          }
        }

        return new Matrix<T>(vec);
      };

      static Matrix<T>* from_generator(
        std::size_t rows,
        std::size_t columns,
        std::function<T(std::size_t, std::size_t)> func
      ) {
        std::vector<std::vector<T>> matrix = {};

        for (std::size_t i = 0; i < rows; i++) {
          matrix.push_back(std::vector<T>());

          for (std::size_t j = 0; j < columns; j++) {
            matrix.at(i).push_back(func(rows, columns));
          }
        }

        return new Matrix<T>(matrix);
      }

      static Matrix<T>* empty() {
        return new Matrix<T>();
      }

      void push_row(std::vector<T> vec) {
        if (vec.size() == 0) {
          throw std::invalid_argument("Cannot push empty vector!");
        }

        if (vec.size() != this->_column_size && this->_column_size != 0) {
          throw std::invalid_argument("Rows should have same length!");
        }

        if (this->_column_size == 0) {
          this->_column_size = vec.size();
        }

        this->_matrix.push_back(vec);
      };

      void push_column(std::vector<T> column) {
        std::size_t height = this->get_height();

        if (column.size() == 0) {
          throw std::invalid_argument("Cannot push empty vector!");
        }

        if (height != column.size() && height != 0) {
          throw std::invalid_argument("Column must have same length!");
        }

        for (; height < column.size(); height++) {
          this->_matrix.push_back(std::vector<T>());
        }

        for (std::size_t row = 0; row < this->_matrix.size(); row++) {
          this->_matrix.at(row).push_back(column.at(row));
        }

        this->_column_size++;
      };
      
      std::vector<T> get_row(std::size_t index) {
        return this->_matrix.at(index);
      };

      std::vector<T> get_column(std::size_t index) {
        std::vector<T> result = {};

        for (std::size_t i = 0; i < this->_matrix.size(); i++) {
          result.push_back(this->get_row(i).at(index));
        }

        return result;
      };

      T at(std::size_t row, std::size_t column) {
        return this->_matrix.at(row).at(column);
      }

      std::size_t get_height() {
        return this->_matrix.size();
      }

      std::size_t get_width() {
        return this->_column_size;
      }

      std::string stringify() {
        std::stringstream result_stream;
        
        for (std::size_t i = 0; i < this->_matrix.size(); i++) {
          result_stream << "│";
          for (std::size_t j = 0; j < this->_column_size; j++) {
            result_stream << helpers::string_center(this->at(i, j), 1);
            if (this->_column_size - j != 1) {
              result_stream << " ";
            }
          }

          result_stream << "│\n";
        }

        return result_stream.str();
      }
  };
}
