#pragma once

#include <memory>

class Parser {
  public:
    Parser();

    template<typename TOutput>
    std::unique_ptr<TOutput> parse(std::string str);
};
