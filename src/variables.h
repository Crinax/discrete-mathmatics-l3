#pragma once

#include <vector>
#include <memory>
#include "adapters/adjecency_matrix.h"

namespace app {
  inline auto input_matrix = adapters::create_adjecency_matrix();
}
