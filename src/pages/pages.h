#pragma once

#include <memory>
#include <vector>
#include <functional>

namespace pages {
  void main_page();
  void input_matrix();
  void convert_to_overtypes();
  void read_from_file();

  template<typename... Args>
  void switch_page(int page, Args... args) {
    switch (page) {
      case 0:
        pages::main_page(args...);
        break;

      case 1:
        pages::input_matrix(args...);
        break;

      case 2:
        pages::read_from_file(args...);
        break;

      case 3:
        pages::convert_to_overtypes(args...);
        break;
    }
  }
}
