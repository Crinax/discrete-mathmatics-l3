#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "pages.h"
#include "../console/clear.h"
#include "../variables.h"
#include "../helpers/string_split/string_split.h"

void pages::input_matrix() {
  console::clear();

  std::size_t matrix_size;

  std::cout << "Введите ранг матрицы: ";
  std::cin >> matrix_size;

  std::cout << "\nВведите матрицу: \n";

  std::flush(std::cout);
  std::cin >> std::ws;
  for (std::size_t i = 0; i < matrix_size; i++) {
    std::string input;
    std::getline(std::cin, input);
    app::input_matrix->push_row(
      helpers::split<std::size_t>(input, ' ')
    );

  }

  pages::switch_page(0); 
}

