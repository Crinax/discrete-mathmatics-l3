#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>
#include <memory>
#include "../console/prompt.h"
#include "../console/clear.h"
#include "../helpers/string_split/string_split.h"
#include "../variables.h"
#include "pages.h"

void pages::read_from_file() {
  std::unique_ptr<std::string> path_to_file;
  std::string _;

  while (true) {
    console::clear();

    path_to_file = console::prompt<std::string>("Введите название файла: ");
    bool is_exists = std::filesystem::exists({ *path_to_file });

    if (is_exists) {
      break;
    }

    std::cout << "Файла не существует. Проверьте правильность пути" << std::endl;
    std::getline(std::cin, _);
  }

  std::ifstream file(*path_to_file);

  if (!file.is_open()) {
    std::cout << "Ошибка открытия файла :(" << std::endl;
  } else {
    while (true) {
      std::string input;
      std::getline(file, input);

      if (file.eof()) {
        break;
      }

      app::input_matrix->push_row(
        helpers::split<std::size_t>(input, ' ')
      );
    }
    std::cout << "Матрица прочитана успешно!" << std::endl;
  }

  std::cout << "Нажмите <Enter> для возврата...";
  std::getline(std::cin, _);
  return pages::switch_page(0);
}
