#include <iostream>
#include <string>
#include "../console/clear.h"
#include "../adapters/converters.h"
#include "../helpers/vector_join/vector_join.h"
#include "pages.h"
#include "../variables.h"

void pages::convert_to_overtypes() {
  console::clear();

  std::string wait;
  auto adj_list = adapters::converters::convert_adjacency_matrix_to_list(
    app::input_matrix
  );
  auto converted = adapters::converters::convert_adjecency_matrix_to_incidence(
    app::input_matrix
  );
  auto inc_list = adapters::converters::convert_incidence_matrix_to_list(
    converted
  );
  std::vector<std::string> adj_list_str = {};
  std::vector<std::string> inc_list_str = {};

  for (std::size_t row = 0; row < adj_list->size(); row++) {
    std::string row_str = std::to_string(row + 1) + ": ";
    adj_list_str.push_back(
      row_str + helpers::vector_join(adj_list->at(row), " -> ")
    );
    inc_list_str.push_back(
      row_str + helpers::vector_join(inc_list->at(row), " -> ")
    );
  }

  std::cout << "Матрица смежности:\n";
  std::cout << app::input_matrix->stringify() << "\n";
  std::cout << "Матрица инциденций:\n";
  std::cout << converted->stringify() << "\n";
  std::cout << "Список смежности:\n";
  std::cout << helpers::vector_join(adj_list_str) << "\n\n";
  std::cout << "Список инциденций:\n";
  std::cout << helpers::vector_join(inc_list_str) << "\n";

  std::cout << "Нажмите <Enter> для возврата...";
  std::getline(std::cin, wait);
  pages::switch_page(0);
}
